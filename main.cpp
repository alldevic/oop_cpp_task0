#include <iostream>
#include <fstream>
#include <list>
#include "MySort.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        std::cout << "Incorrect input args!" << std::endl;
        std::cout << "Use \"oop_cpp_task0.exe <input file> <output file>\" without quotes" << std::endl;
        return 1;
    }

    std::string tmpStr;
    std::list<std::string> strList;

    std::ifstream inputFile(argv[1], std::fstream::in);
    while (inputFile.good()) {
        std::getline(inputFile, tmpStr);
        strList.push_front(tmpStr);
    }
    inputFile.close();


    MySort::sort_strings(strList);

    std::ofstream outputFile(argv[2], std::fstream::out);
    if (outputFile.is_open()) {
        for (const std::string &i: strList) {
            outputFile << i << std::endl;
        }
    }
    outputFile.close();

    return 0;
}
