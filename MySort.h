#ifndef MYSORT_H
#define MYSORT_H

#include <string>
#include <list>

namespace MySort {
    void sort_strings(std::list<std::string> lst);
}

#endif //MYSORT_H