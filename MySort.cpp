#include "MySort.h"

/**
 * Function for sorting List<string> via standard std::list function
 * @param lst - List<string> for sorting
 */
void MySort::sort_strings(std::list<std::string> lst) {
    lst.sort();
}
